$user='ctp'

User["$user"] -> Class['nrgctp']

common::mkdir_p { "/home/${$user}/.ssh": }
file { 
  "/home/${$user}/.ssh" :
    ensure  => directory,
    owner   => $user,
    require => Common::Mkdir_p["/home/${$user}/.ssh"],
}


user { "$user": 
  ensure => present, 
  home => "/home/${user}",
  managehome => true,
  shell      => '/bin/bash',
  require => File["/home/${$user}/.ssh"],
}

ssh_authorized_key { 'ssh_id_rsa_for_relay':
  ensure => present,
  user => $user,
  type => 'ssh-rsa',
  # key's value should be from NRG_secure.eyaml -- generic::credentials::relaynucid_rsa_id_pub
  key => '',
  require => User["$user"],
}

class { 'nrgctp':
  file_root => "/data/ctp/",
  ctp_repo => { 
    source => 'git@bitbucket.org:nrgpuppeteers/ctp_shared.git', 
    provider => 'git',
    identity => '/home/relay_nrg/.ssh/id_relay_workstation',
  },
  ctp_config_repo => { 
    source => 'git@bitbucket.org:nrgpuppeteers/ctp_config.git', 
    provider => 'git', 
    identity => '/home/relay_nrg/.ssh/id_relay_workstation', 
  },
}