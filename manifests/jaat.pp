# == Class: jaat
#
# This class pulls four repos (nrgpuppeteers/jaat*.* and nrgpuppeteers/ctp*.*) and clones them in /opt/. Also,
#   it creates CRON jobs for JAAT user relevent to each machine's fqdn.
#
# @param [Hash] jaat_shared_repo A hash representing the repo containing JAAT application files,
#   to be passed create_resources('vcsrepo').
# @param [Hash] jaat_config_repo A hash representing the repo containing machine-specific
#   config files for JAAT, to be passed to create_resources('vcsrepo').
# @param [String] jaat_user Name of user under which JAAT application runs, assumes this
#   user resource exists and is managed by another class, e.g. profile::domain_accounts.
# @param [String] jaat_root Root directory for JAAT user which contains two repos which are pulled from nrgpuppeteers.
#   nrgpuppeteers/jaat_config clones to ${jaat_root}/config_repo and nrgpuppeteers/jaat_shared clones to ${jaat_root}/shared_repo.
# @param [String] jaat_log_dir Loggin directory for JAAT user.
# @param [String] ctp_root Root directory for CTP user which contains two repos which are pulled from nrgpuppeteers.
#   nrgpuppeteers/ctp_config clones to ${ctp_root}/repo and nrgpuppeteers/ctp_shared clones to ${ctp_root}/shared_repo.
# @param [String] ctp_user  Name of user under which CTP application runs, assumes this
#   user resource exists and is managed by another class, e.g. profile::domain_accounts.
# @param [String] ctp_group The group under which CTP process runs.
# @param [Hash] ctp_shared_repo A hash representing the repo containing CTP application files,
#   to be passed create_resources('vcsrepo').
# @param [Hash] ctp_config_repo_nrgpuppeteers A hash representing the repo containing machine-specific
#   config files for CTP, to be passed to create_resources('vcsrepo').
# @param [Boolean] jaat_run_once Whether to run this class once at machine deployment and thereafter
#   disable itself.
# @param jaat_nopuppet If this hiera variable/fact is true, this class will do nothing.
#   The *jaat_run_once* parameter above causes this fact to be set true on first execution.
# @author NRG nrg-admin@nrg.wustl.edu Copyright 2018
#
class nrgctp::jaat (
  String $jaat_user = 'nrg-svc-cnda-jaat',
  String $jaat_group = 'nrg-cnda-ctp',
  String $jaat_root = '/opt/jaat',
  String $jaat_log_dir = '/var/log/jaat',
  Boolean $jaat_run_once = false,
  Hash $jaat_shared_repo,
  Hash $jaat_config_repo,
  ) {

Class['nrgctp'] -> Class['nrgctp::jaat']

Vcsrepo["${jaat_root}/shared_repo"] -> Vcsrepo["${jaat_root}/config_repo"]

  # # Resolve domain & remote users
  # include profile::domain_accounts
  # # Deposit user SSH keys
  # include profile::ssh::userconfig
  include nrgctp

  # This module gated by $::jaat_nopuppet fact
  if ! $::jaat_nopuppet {

    # Class['profile::ssh::userconfig'] -> Class['nrgctp'] -> Class['jaat']
  # Create two cron jobs for JAAT user for weekdays and weekends. The scripts pulls jaat_config_repo  
    # cron {
    #   'runWeekdaysJaat':
    #     ensure  => 'present',
    #     command => "${jaat_root}/config_repo/${::fqdn}/cron/runNightlyJaat.sh",
    #     hour    => '19',
    #     minute  => '00',
    #     weekday => ['Monday','Tuesday','Wednesday','Thursday','Friday'],
    #     user    => $jaat_user,
    #     require => [ Class['nrgctp'], User[$jaat_user] ];
    #   'runWeekendsJaat':
    #     ensure  => 'present',
    #     command => "${jaat_root}/config_repo/${::fqdn}/cron/runNightlyJaat.sh",
    #     hour    => '00',
    #     minute  => '05',
    #     weekday => ['Saturday','Sunday'],
    #     user    => $jaat_user,
    #     require => [ Class['nrgctp'], User[$jaat_user] ];
    # }

    file { $jaat_log_dir:
      ensure  => directory,
      owner   => $jaat_user,
      group   =>  $nrgctp::ctp_group,
      mode    => '0755',
      require => [ Class['nrgctp']],
    }

    # Create jaat_root
    common::mkdir_p { "${jaat_root}": }
    file { "${jaat_root}" :
      ensure  => directory,
      owner   => $jaat_user,
      group   => $jaat_group,
      mode    => '6755',
      require => [Common::Mkdir_p["${jaat_root}"], Class['nrgctp']]
    }

    # Now declare vcsrepo resources to check out repos to relevant destinations
    $jaat_repo_options = {
      'ensure'   => latest,
      'provider' => 'git',
      'user'     => $jaat_user,
      'group'     => $jaat_group,
      'require'  => [Class['nrgctp'], File[$jaat_root], File["${jaat_root}/shared_repo"], File["${jaat_root}/config_repo"]],
    }

    file {
      "${jaat_root}/shared_repo":
        ensure  => 'directory',
        owner   => $jaat_user,
        group => $jaat_group,
        require => File[$jaat_root];
      "${jaat_root}/config_repo":
        ensure  => 'directory',
        owner   => $jaat_user,
        group => $jaat_group,
        require => File[$jaat_root];
    }


    $repos_hash_jaat = {
      "${jaat_root}/shared_repo" => $jaat_shared_repo,
      "${jaat_root}/config_repo" => $jaat_config_repo,
    }


    # creating jaat repo
    create_resources('vcsrepo', $repos_hash_jaat, $jaat_repo_options)

  } else {
    notify { 'jaat class disabled, fact jaat_nopuppet=true.': }
  }

  # Set jaat_nopuppet fact if jaat_run_once is enabled
  ensure_resource('common::mkdir_p', '/etc/facter/facts.d')
  $jaat_nopuppet_ensure = $jaat_run_once ? {
    true    => 'present',
    default => 'absent',
  }
  file { '/etc/facter/facts.d/jaat_nopuppet.txt':
    ensure  => $jaat_nopuppet_ensure,
    content => 'jaat_nopuppet=true',
    require => Common::Mkdir_p['/etc/facter/facts.d'],
  }


  #exec {
    # 'rsyncJaatConfigRepo':
    #   command   => "/usr/bin/rsync -a --exclude=\'.git/\' ${jaat_root}/config_repo/${fqdn}/ ${jaat_root}/ --delete",
    #   logoutput => true,
    #   user      => $jaat_user;
    # 'rsyncJaatSharedRepo':
    #   command   => "/usr/bin/rsync -a --exclude=\'.git/\' ${jaat_root}/shared_repo/ ${jaat_root}/",
    #   logoutput => true,
    #   user      => $jaat_user;
    # 'changeJaatOwnerShip':
    #   command   => "/usr/bin/chown -R ${jaat_user}:${nrgctp::ctp_group} ${jaat_root}/",
    #   logoutput => true;
  #}
}
