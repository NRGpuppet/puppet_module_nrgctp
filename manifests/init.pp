# Class: nrgctp
# ===========================
#
# Full description of class nrgctp here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'nrgctp':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2018 Your name here, unless otherwise noted.
#

class nrgctp (
  String $ctp_user = 'nrg-svc-cnda-ctp',
  String $ctp_group = 'nrg-cnda-ctp',
  String $ctp_log_dir = "/var/log/ctp",
  Optional[String] $ctp_java = undef,
  String $ctp_runner_java_max_memory = '4096',
  String $ctp_runner_java_min_memory = '1024',
  Variant[Numeric, String] $ctp_memory_start = '1024',
  Variant[Numeric, String] $ctp_memory_max = '4096',
  Variant[Numeric, String] $ctp_web_port = '1080',
  # String  $ctp_root = '/ctp-default-root',
  Variant[Numeric, String] $dicom_import_port = '1104',
  Variant[Numeric, String] $storage_service_web_port = '1085',
  Hash $ctp_repo,
  Hash $ctp_config_repo,
  String $ctp_config_path = '/opt/config',
  String $ctp_shared_path = '/opt/shared',
  String $ctp_path = '/JavaPrograms/CTP',
  Boolean $is_ctp_root_mount_point = false,
  # Optional[Array] $augeas_users_xml = undef,
  # Optional[Array] $augeas_config_xml = undef,
  ){


  include profile::ctp

  # include java
  $ctp_java_real =$ctp_java ? {
    undef   => $::java_default_home,
    default => $ctp_java,
  }

  # $repo_user = $ctp_user

  common::mkdir_p { "${ctp_path}": }
  file { "${ctp_path}" :
    ensure  => directory,
    owner   => $ctp_user,
    group => $ctp_group,
    mode    => '2750',
    require => Common::Mkdir_p["${ctp_path}"];
  }

  common::mkdir_p { "${ctp_shared_path}": }
  file { "${ctp_shared_path}" :
    ensure  => directory,
    owner   => $ctp_user,
    group   => $ctp_group,
    mode    => '2750',
    require => Common::Mkdir_p["${ctp_shared_path}"];
  }

  common::mkdir_p { "${ctp_config_path}": }
  file { "${ctp_config_path}" :
    ensure  => directory,
    owner   => $ctp_user,
    group   => $ctp_group,
    mode    => '2750',
    require => Common::Mkdir_p["${ctp_config_path}"];
  }


  $ctp_repo_options = { # default repo options
    ensure   => latest,
    provider => 'git',
    user     => $ctp_user,
    group    => $ctp_group,
    require  => [File[$ctp_shared_path], File[$ctp_config_path]],
  }

  $ctp_repo_hash = {
    "$ctp_shared_path" => $ctp_repo,
    "$ctp_config_path" =>  $ctp_config_repo,
  }

  create_resources('vcsrepo', $ctp_repo_hash, $ctp_repo_options)

  File["${ctp_shared_path}"] ~> File["${ctp_config_path}"] 
  ~> Vcsrepo["${ctp_shared_path}"] ~> Vcsrepo["${ctp_config_path}"]
  ~> Exec["changeOwnerShip"]
  ~> File["${ctp_path}"]
  # ~> Augeas['ctp_config_users_xml']
  # ~> Service['ctp']

  # augeas { 'ctp_config_users_xml':
  #     incl    => "${ctp_path}/users.xml",
  #     lens    => 'Xml.lns',
  #     context => "/files/${ctp_path}/users.xml/users",
  #     changes => $augeas_users_xml,
  # }

  if $service_provider == 'systemd' {
    file {
      '/usr/lib/systemd/system/ctp.service':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("nrgctp/ctp.service.erb");
    } ~>
    exec {
      "reload_systemctl":
        command     => "/usr/bin/systemctl daemon-reload",
        refreshonly => true;
    }
    service {
      "ctp":
        enable    => true,
        ensure    => running,
        subscribe =>  File['/usr/lib/systemd/system/ctp.service'],
    }
  } else {
    file {
      "/etc/init.d/ctp":
        ensure => file,
        source => "puppet:///modules/nrgctp/ctp",
        owner  => "root",
        group  => "root",
        mode   =>  "0755";
    }

    service {
      "ctp":
        enable  => true,
        ensure  => running,
        status  => "/sbin/service ctp status",
        restart =>   "/bin/true",
        require => [ File["/etc/sysconfig/ctp", "/etc/init.d/ctp", "$ctp_log_dir"] ];
    }
  }

  exec {
    'changeOwnerShip':
      command   => "/usr/bin/chown -R ${ctp_user}.${nrgctp::ctp_group} ${ctp_shared_path}/ ;
      /usr/bin/chown -R ${ctp_user}.${nrgctp::ctp_group} ${ctp_config_path}/ ";
  #   'create_ctp_root':
  #     command   => "/usr/bin/mkdir -p $ctp_root",
  #     logoutput => true,
  #     creates   =>   "$ctp_root",
  #     # create ctp root directory only if, is is not exist
  #     onlyif    => "test ! -d $ctp_root";
  #   # 'restart_ctp_if_new_files_repo':
  #   #   provider  => shell,
  #   #   user => $ctp_user,
  #   #   logoutput => true,
  #   #   command => "
  #   #     rsync -r  --exclude=\'.git/\' ${ctp_shared_path}/ ${ctp_path}/ --delete;
  #   #     rsync -r --exclude=\'.git/\' ${ctp_config_path}/${fqdn}/${ctp_path}/ ${ctp_path}/ ;
  #   #   "
  }


# # create directory only if the path is not mount point
#   if $is_ctp_root_mount_point {
#     file { "$ctp_root":
#       owner   => $ctp_user,
#       group   => $ctp_group,
#       mode    => "2750",
#       ensure  => directory,
#       recurse => true,
#     }
#   } else {
#     file { "$ctp_root":
#       owner   => $ctp_user,
#       group   => $ctp_group,
#       mode    => "2750",
#       ensure  => directory,
#       recurse => true,
#       require => Exec["create_ctp_root"];
#     }
#   }

  file {
    "/etc/sysconfig/ctp":
      ensure  => file,
      content => template("nrgctp/ctp.erb"),
      owner   => "root",
      group   => "root",
      mode    => "0640",
      notify  => Service["ctp"];
    "$ctp_log_dir":
      ensure => directory,
      owner  => $ctp_user,
      group  => "root",
      mode   => "0750";
    # "${ctp_path}/Launcher.properties":
    #   ensure  => present,
    #   owner   => $ctp_user,
    #   group => $ctp_group,
    #   mode => "0640",
    #   content =>   template("nrgctp/Launcher.properties.erb"),
    #   notify  =>   Service["ctp"];
  }

  firewall {
    '100 CTP web port':
      dport   => $ctp_web_port,
      ctstate => 'NEW',
      proto   => tcp,
      action  => accept,
      chain   => 'INPUT';
    '100 DICOM import port':
      dport   => $dicom_import_port,
      ctstate => 'NEW',
      proto   => tcp,
      action  => accept,
      chain   => 'INPUT';
    '100 Storage Service Web':
      dport   => $storage_service_web_port,
      ctstate => 'NEW',
      proto   => tcp,
      action  => accept,
      chain   => 'INPUT';
  }

}
